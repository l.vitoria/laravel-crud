<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//não esquecer de importar a model
use App\Client;

class ClientsController extends Controller
{   
    public function listar(){
        $clients = Client::all();
        return view('admin.cliente.list', compact('clients'));
    }

    public function formcadastrar(){
        return view('admin.cliente.create');

    }

    public function cadastrar(Request $request){
        $client = new client();
        $client->name = $request->name;
        $client->email = $request->email;
        //dd($client);
        $client->save();
        //return redirect()->to('admin/client');
        return redirect('admin/client');
    }

    public function formeditar($id){
        $client = Client::find($id);
        if(!$client){
            abort(404);
        }
        return view('admin.cliente.edit', compact('client'));
    }

   
    public function editar(Request $request, $id){
        $client = Client::find($id);
        if(!$client){
            abort(404);
        }
        $client->name = $request->name;
        $client->email = $request->email;
        //dd($client);
        $client->save();
        //return redirect()->to('admin/client');
        return redirect('admin/client');
    }

    public function excluir(Request $request, $id){
        $client = Client::find($id);
        if(!$client){
            abort(404);
        }
        $client->delete();
        return redirect('admin/client');
    }


     /*public function cadastrar(){
        $nome = "lucas";
        $idade = 24;
        return view('test')
            ->with('nome', $nome)
            ->with('idade', $idade)
            ->with('test', 'outro valor');
    }*/
}
