<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'clients';
    //pode se percebe que a na migrate a tabela esta com s, 
    //mas a model sem o laravel ele mesmo coloca o s, mas 
    //tome cuidado pois no ingles as vezes muda por exemplo category fica categories
}
