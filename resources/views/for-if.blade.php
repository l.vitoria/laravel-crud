<h1>for if</h1>
@if($value>100)
    <p>maior que 100</p>
@else
    <p>menor que 100</p>
@endif

@for($i=0;$i<=$value;$i++)
    -{{$i}}
@endfor


<br/><br/><br/><br/><br/>
<!-- abriu o php e depois abriu  o while -->
@php 
    $i = 0;
@endphp

@while ($i<=$value)
        -{{$i}}
    @php
        $i++;
    @endphp
    
@endwhile


<br/><br/><br/><br/><br/>
<!-- o loop pega o index do foreach
    key pega o index do array que eu mandei 
    o value e o valor que eu mandei -->
@foreach ($myarray as $key=>$value)
    <p>{{$loop->index}}-{{$key}} - {{$value}}</p>
@endforeach

<br/><br/>
<!-- quando não tem nada no array -->
@forelse ([] as $key=>$value)
    <p>{{$loop->index}}-{{$key}} - {{$value}}</p>
@empty
    <p>nenhum registor encontrado</p>
@endforelse
