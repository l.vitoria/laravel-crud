<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>cadastrar cliente</h1>
    <form action="/admin/client/cadastrar" method="post">
       <!-- <input type="hidden" name="_token" value="$csrfToken"> -->
       {!! csrf_field() !!}
        <label for="name">nome</label>
        <input type="text" name="name" id="name">

        <label for="email">email</label>
        <input type="email" name="email" id="email">
        <button type="submit">cadastrar</button>
    </form>
    
</body>
</html>