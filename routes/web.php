<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use \Illuminate\Http\Request;

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => '/'], function () {
    Route::get('cliente/cadastrar', 'ClientsController@cadastrar');
});

Route::group(['prefix' => '/admin'], function () {
    Route::get('/client', 'ClientsController@listar');
    Route::get('/client/formcadastrar', 'ClientsController@formcadastrar');
    Route::post('/client/cadastrar', 'ClientsController@cadastrar');
    Route::get('/client/{id}/formeditar', 'ClientsController@formeditar');
    Route::post('/client/{id}/editar', 'ClientsController@editar');
    Route::get('/client/{id}/excluir', 'ClientsController@excluir');
});





Route::get('/for-if/{value}', function ($value) {
    return view('for-if')
        ->with('value', $value)
        ->with('myarray', [
                'chave1' => 'valor1',
                'chave2' => 'valor2',
                'chave3' => 'valor3'
                ]);
    });


Route::get('/blade', function () {
    $nome = "lucas";
    $idade = 24;
    return view('test')
        ->with('nome', $nome)
        ->with('idade', $idade)
        ->with('test', 'outro valor');
});





















/*
pegando um valor dos inputs

Route::get('/cliente', function () {
    //csrf-token
    $csrToken = csrf_token();
    $html = <<<HTML
      <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <title>Document</title>
        </head>
        <body>
            <h1>cliente</h1>
            <form action="/cliente/cadastrar" method="post">
                <input type="hidden" name="_token" value="$csrToken"> 
                <input type="text" name="name" id="">
                <button type="submit">enviar</button>
            </form>
        </body>
        </html>
HTML;

return $html;
});

// sempre import use \Illuminate\Http\Request;
Route::post('/cliente/cadastrar', function (Request  $resquest) {
    // dessas duas maneiras pega os valores vindo pelos inputs
   echo  $resquest->get('name');
   //echo $resquest->name;
});



*/







/*
Route::get('/produto/{name}/{id}', function ($name,$id) {
    return "produto $name - $id";
});

// segundo parameto de entrada opcional 
Route::get('/fornecedor/{name}/{id?}', function ($name,$id = 10000) {
    return "fornecedor $name - $id";
});
*/